# Stellar Evicting Data Cache

## How to Run
The web server can be run locally via the following gradle command:
```bash
./gradlew bootRun
```

This will make the server available on `localhost:8080/snippets`, or `127.0.0.1:8080/snippets`.

## Architecture
I started this application with a Java Spring Boot [initialized](https://start.spring.io/) app.

This gave me an out of the box bootable jar, along with several nice facilities for exposing RESTful endpoints.

I decided to store `Snippet` objects (transformed from the web request) in a `SnippetCache`, which allowed for lookups of keys, and additions of new `Snippets`.

### Likes

For likes, I decided to go with a separate `ConcurrentHashMap` (a map that ensures `computeIfPresent` and several operations are atomic (let's say if we wanted to expose this application to run on multiple threads for performance).

The idea behind this separate map is that now liking of snippets is a.) atomic (in case of viral posts getting liked with significant contention), and b.) we don't have to lock the entire Snippet object to do so.

While the `likes` API returns the entire `Snippet`, and all other endpoints return `likes`, it's feasible these could be split out in future requirements, or `Likes` might even be enriched to include the "like-ee", when it was liked, etc., which provides further benefit in splitting out these concepts from one another. 

## Assumptions

The main assumption here was that cleanup/eviction of the cache should not be done synchronously with other user activity. In a real world application, I would either decide to employ a persistent cache with built-in eviction policy handling, or would create a separate thread responsible for waking up every few seconds to clean up expired entries.

However, for the purposes of this application, I decided to leave expired entries around forever, unless they are overwritten by new values.

I assumed further that redundant attempts to put a Snippet in the cache should raise an exception (in this case, I chose an HTTP Status of 423).

## Cleanup

Things I would've loved to get to with some time would be:

- Unit testing around the cache
- Integration testing around the controllers with an API mocking library
- Cleanup of some of the non-DRY elements in the cache as of the time of writing.