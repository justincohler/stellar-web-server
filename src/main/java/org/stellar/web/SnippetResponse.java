package org.stellar.web;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.time.Instant;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
class SnippetResponse {

    private static final String URL_PREFIX = "/snippets/";
    private String url;
    private String name;
    private Instant expiresAt;
    private String snippet;
    private Integer likes;

    private SnippetResponse() {
    }

    static SnippetResponse from(Snippet snippet, int likes) {
        return new SnippetResponse(
                URL_PREFIX + snippet.getName(),
                snippet.getName(),
                snippet.getExpiresAt(),
                snippet.getSnippet(),
                likes
        );
    }

    private SnippetResponse(String url, String name, Instant expiresAt, String snippet, Integer likes) {
        this.url = url;
        this.name = name;
        this.expiresAt = expiresAt;
        this.snippet = snippet;
        this.likes = likes;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public Instant getExpiresAt() {
        return expiresAt;
    }

    public String getSnippet() {
        return snippet;
    }

    public Integer getLikes() {
        return likes;
    }
}
