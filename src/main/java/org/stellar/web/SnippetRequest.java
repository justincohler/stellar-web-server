package org.stellar.web;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
class SnippetRequest {

    private String name;
    private Integer expiresIn;
    private String snippet;

    private SnippetRequest() {
    }

    private SnippetRequest(String name, Integer expiresIn, String snippet) {
        this.name = name;
        this.expiresIn = expiresIn;
        this.snippet = snippet;
    }

    public String getName() {
        return name;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public String getSnippet() {
        return snippet;
    }
}
