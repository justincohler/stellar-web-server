package org.stellar.web;

import java.time.Instant;

public class Snippet {

    private final String name;
    private Instant expiresAt;
    private final String snippet;

    public Snippet(String name, Instant expiresAt, String snippet) {
        this.name = name;
        this.expiresAt = expiresAt;
        this.snippet = snippet;
    }

    public String getName() {
        return name;
    }

    public Instant getExpiresAt() {
        return expiresAt;
    }

    public String getSnippet() {
        return snippet;
    }

    public void extend(int extensionSeconds) {
        expiresAt = expiresAt.plusSeconds(extensionSeconds);
    }
}
