package org.stellar.web;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.time.Instant.now;

@Repository
public class SnippetCache {

    private static final int EXTENSION_SECONDS = 30;

    private final Map<String, Snippet> cache = new HashMap<>();
    private final Map<String, Integer> likes = new ConcurrentHashMap<>();


    void putSnippet(Snippet snippet) {
        String key = snippet.getName();
        cache.computeIfPresent(key, (k, value) -> {
            if (value.getExpiresAt().isAfter(now())) {
                throw new IllegalArgumentException("Already have a snippet of this name reserved.");
            }
            return snippet;
        });
        cache.putIfAbsent(key, snippet);
    }

    Optional<Snippet> getSnippet(String name) {
        if (!cache.containsKey(name) || cache.get(name).getExpiresAt().isBefore(now())) {
            return Optional.empty();
        }

        Snippet snippet = cache.get(name);
        snippet.extend(EXTENSION_SECONDS);
        return Optional.of(snippet);
    }

    Optional<Snippet> like(String name) {
        if (!cache.containsKey(name) || cache.get(name).getExpiresAt().isBefore(now())) {
            return Optional.empty();
        }
        Snippet snippet = cache.get(name);
        snippet.extend(EXTENSION_SECONDS);

        likes.computeIfPresent(name, (key, value) -> value + 1);
        likes.putIfAbsent(name, 1);

        return Optional.of(snippet);
    }

    int getLikes(String name) {
        return likes.getOrDefault(name, 0);
    }
}
