package org.stellar.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;

@RestController
public class SnippetController {

    private final SnippetCache snippetCache;

    @Autowired
    public SnippetController(SnippetCache snippetCache) {
        this.snippetCache = snippetCache;
    }

    @GetMapping("/snippets/{name}")
    @ResponseBody
    public SnippetResponse getSnippet(@PathVariable String name) {

        return snippetCache.getSnippet(name)
                .map(snippet -> SnippetResponse.from(snippet, snippetCache.getLikes(name)))
                .orElseThrow(() ->
            new ResponseStatusException(
                    HttpStatus.NOT_FOUND
            )
        );
    }

    @PostMapping("/snippets")
    @ResponseBody
    public SnippetResponse create(@RequestBody SnippetRequest request) {

        Snippet snippet = new Snippet(request.getName(), Instant.now().plusSeconds(request.getExpiresIn()), request.getSnippet());

        try {
            snippetCache.putSnippet(snippet);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(
                    HttpStatus.LOCKED
            );
        }
        return SnippetResponse.from(snippet, snippetCache.getLikes(snippet.getName()));
    }


    @PostMapping("/snippets/{name}/like")
    @ResponseBody
    public SnippetResponse getLikes(@PathVariable String name) {

        return snippetCache.like(name)
                .map(snippet -> SnippetResponse.from(snippet, snippetCache.getLikes(name)))
                .orElseThrow(() ->
                        new ResponseStatusException(
                                HttpStatus.NOT_FOUND
                        )
                );
    }
}